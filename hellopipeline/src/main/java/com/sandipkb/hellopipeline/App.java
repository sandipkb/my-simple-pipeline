package com.sandipkb.hellopipeline;

import com.sandipkb.secondproject.calculator.SimpleInterest;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        System.out.println( "<<<<Hello World with Gitlab>>>>>!" );
        System.out.println("Calculated simpleinterest : " + SimpleInterest.interest(10000, 10, 5));
    }
}
